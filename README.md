# multi-ecg-line-webcomponent


Simple class webcomponent that draws an ECG line. With ability to add multiple lines to a single page.

All credit goes to [tripolskypetr](https://github.com/tripolskypetr) for creating [ecg-line-webcomponent](https://github.com/tripolskypetr/ecg-line-webcomponent) this repo converts his code to class based so that multiple ECG's can be added.

![Example Page](./multi-ecg-webcomponent-demonstration.gif)


