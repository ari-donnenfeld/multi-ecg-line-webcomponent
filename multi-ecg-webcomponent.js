class EcgLine extends HTMLElement {
    constructor() {
        super();
        const HEIGHT = this.getAttribute('height') || 125;
        const WIDTH = this.getAttribute('width') || 275;
        const INTERVAL = 30;
        const CURVE_COLOR = "#22ff22";
        this.heartData = new Array(100).fill(0); // Initial this.heartData with length 100
        this.heartDataIndex = 0;
        this.beatDataIndex = -1;
        this.BANG = false;

        const fillBeatData = () => {
            const getValue = idx => {
                switch (idx) {
                    case 0:
                        return Math.random() * 0.1 + 0.1;
                    case 1:
                        return Math.random() * 0.1 + 0.0;
                    case 2:
                        return Math.random() * 0.3 + 0.7;
                    case 3:
                        return Math.random() * 0.1 - 0.05;
                    case 4:
                        return Math.random() * 0.3 - 0.8;
                    case 5:
                    case 6:
                        return Math.random() * 0.1 - 0.05;
                    case 7:
                        return Math.random() * 0.1 + 0.15;
                    default:
                        return 0;
                }
            };
            this.heartData[this.heartDataIndex] = getValue(this.beatDataIndex);
            this.beatDataIndex++
            if (this.beatDataIndex > 7) {
                this.beatDataIndex = -1;
            }
        };

        const fillRandomData = () => {
            this.heartData[this.heartDataIndex] = Math.random() * 0.05 - 0.025;
        };

        const updateData = () => {
            this.heartDataIndex = (this.heartDataIndex + 1) % this.heartData.length;
            if (this.beatDataIndex >= 0 || this.BANG) {
                fillBeatData();
                this.BANG = false;
            } else {
                fillRandomData();
            }
        };

        const ellipse = (ctx, x, y, a, b) => {
            ctx.save();
            ctx.beginPath();
            ctx.translate(x, y);
            ctx.scale(a / b, 1);
            ctx.arc(0, 0, b, 0, Math.PI * 2, true);
            ctx.restore();
            ctx.closePath();
        };

        const onPaint = () => {
            const canvas = this.canvas;
            const context = canvas.getContext('2d');
            context.clearRect(0, 0, WIDTH, HEIGHT);
            const baseY = HEIGHT / 2;
            const length = this.heartData.length;
            const step = (WIDTH - 5) / length;
            const yFactor = HEIGHT * 0.35;
            let heartIndex =  (this.heartDataIndex + 1) % length;
            context.strokeStyle = CURVE_COLOR;
            context.beginPath();
            context.moveTo(0, baseY);
            for (let i = 0; i < length; i++) {
                const x = i * step;
                const y = baseY - this.heartData[heartIndex] * yFactor;
                context.lineTo(x, y);
                heartIndex = (heartIndex + 1) % length;
            }
            context.stroke();
            context.closePath();
            context.beginPath();
            context.fillStyle = CURVE_COLOR;
            ellipse(context, (length - 1) * step, baseY - this.heartData[this.heartDataIndex] * yFactor, 2, 2);
            context.fill();
            context.closePath();
        };

        const interval = setInterval(() => {
            if (document.contains(this.canvas)) {
                updateData();
                onPaint();
            } else {
                clearInterval(interval);
            }
        }, INTERVAL);

        this.canvas = document.createElement('canvas');
        this.canvas.height = HEIGHT;
        this.canvas.width = WIDTH;
        this.appendChild(this.canvas);
    }

    connectedCallback() {
        const callback = fn => fn(() => this.BANG = true);
        window.ecgLine = callback;
    }
}

customElements.define('ecg-line', EcgLine);

